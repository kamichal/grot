#!/ve
# pip install pydantic==1.10.*

def example_network():
    """
    # Network visualization.

    This uses Pydantic dataclasses to define network elements that create
    local area network (LAN) environment visualization.

    Personally, I wrote it to document my LAN arrangement, since I use
    static IP addresses assignments and have much more network nodes than
    this in the example.

    What's interesting - you define port connections by value of `NetworkNode.patch_to`,
    and it's automatically shown in switch ports assignments.
    """
    import os

    from airium import Airium
    from pydantic import BaseModel, Field

    from grot import Grot

    class NetInterface(BaseModel):
        ip: str | None
        patch_to: str | None = None  # name of NetInterface
        route_back: str | None = None  # automatically filled by Lan.fill_route_back_connections()

        def is_connected(self) -> bool:
            return bool(self.patch_to or self.route_back)

    class NetworkNode(BaseModel):
        node_class: str | None
        model: str | None
        interfaces: dict[str, NetInterface] = Field(default_factory=dict)

        # graphviz_subgraph_num: int | None # needed to place the node in graphs

        @classmethod
        def make_switch(cls, num_ports: int = 8, **kwargs) -> "NetworkNode":
            ports = {f"p{i:02}": NetInterface() for i in range(1, num_ports + 1)}
            return cls(**kwargs, node_class="switch", interfaces=ports)

        def make_node_body(self, name: str) -> str:
            """Generate HTML code that fills graph's node details."""
            a = Airium(base_indent="")

            fg_color = {
                "switch": "#b14433",
                "router": "#313131",
            }.get(self.node_class, "#337")
            bg_color = {
                "switch": "#ccc",
                "router": "#eee",
            }.get(self.node_class, "#ddd")

            with a.table(border="0", bgcolor=bg_color):
                with a.tr().td(align="center", colspan=2):
                    with a.font(color=fg_color, **{"point-size": "20"}):
                        a(f"<b><u>{name}</u></b>")
                    with a.font(color=fg_color, **{"point-size": "12"}):
                        a(f" - <i>{self.node_class}</i> - {self.model}")

                self.draw_interfaces(a, fg_color)

            return str(a)

        def draw_interfaces(self, a: Airium, fg_color="#334433"):
            for name, interface in self.interfaces.items():
                interface: NetInterface
                ip_label = interface.ip or interface.patch_to
                if not ip_label:
                    ip_label = f"<i>to {interface.route_back}</i>" if interface.route_back else "-"
                name_label = f"<b>{name}</b>" if interface.is_connected() else name
                with a.tr():
                    a.td(align="center", port=name, bgcolor="#fafbfc").font(_t=name_label, color=fg_color)
                    a.td(align="left").font(_t=f"{ip_label}", color=fg_color)
            if not self.interfaces:
                a.tr().td(colspan=2).font(_t="No interfaces", color=fg_color)

    class Lan(BaseModel):
        nodes: dict[str, NetworkNode] = Field(default_factory=dict)

        def draw_in_grot(self, g: Grot) -> None:
            self.fill_route_back_connections()
            nodes: dict[str, str] = {}
            for name, node_object in self.nodes.items():
                table_body: str = node_object.make_node_body(name)
                nodes[name] = g.html_node(table_body)

            for name, node_object in self.nodes.items():
                for if_name, interface in node_object.interfaces.items():
                    if interface.patch_to:
                        source_fmt = f"{nodes[name]}:{if_name}"
                        src_node, src_interface = split_by_colon(interface.patch_to)
                        graphviz_path = f"{nodes[src_node]}:{src_interface}" if src_interface else f"{nodes[src_node]}"
                        g.g.edge(source_fmt, graphviz_path, arrowhead="none")

        def fill_route_back_connections(self):
            connections = {}
            for name, node_object in self.nodes.items():
                for if_name, if_obj in node_object.interfaces.items():
                    if_obj: NetInterface
                    if if_obj.patch_to:
                        dst_name, dst_port = split_by_colon(if_obj.patch_to)
                        if dst_name not in connections:
                            connections[dst_name] = {}
                        connections[dst_name][dst_port] = f"{name}:{if_name}"

            for name, node_object in self.nodes.items():
                if name in connections:
                    for port_name, port_obj in node_object.interfaces.items():
                        if port_name in connections[name] and not port_obj.route_back:
                            port_obj.route_back = connections[name][port_name]

    def split_by_colon(path_spec: str) -> tuple[str, str | None]:
        if path_spec and ":" in path_spec:
            src_node, src_interface = path_spec.split(":", 1)
        else:
            src_node, src_interface = path_spec, None
        return src_node, src_interface

    environment = Lan(
        # Of course, it makes sense to automatically build this
        # structure by some network diagnostic tool, but this would be beyond
        # the scope of this example. If you are interested, however - let me
        # know maybe I am as well.
        nodes={
            "router": NetworkNode(
                node_class="router",
                model="Sagemcom 1Gbps",
                interfaces={
                    "wan": NetInterface(ip="170.1.12.46"),
                    "gate": NetInterface(ip="192.168.1.1"),
                    "p01": NetInterface(patch_to="switch3:p01"),
                    "p02": NetInterface(),
                    "p03": NetInterface(),
                    "p04": NetInterface(),
                    "WiFi1": NetInterface(ip="Broken-WiFi"),
                    "WiFi2": NetInterface(ip="BYOD"),
                },
            ),
            "switch3": NetworkNode.make_switch(8, model="TpLink"),
            "kapc": NetworkNode(
                node_class="laptop",
                model="TRX1232",
                interfaces={
                    "enp0s3": NetInterface(ip="192.168.1.26", patch_to="switch3:p08"),
                    "wlp1s0": NetInterface(ip="192.168.1.27"),
                },
            ),
            "the_nas": NetworkNode(
                node_class="nas",
                model="QPAN ST241",
                interfaces={
                    "eno1": NetInterface(ip="192.168.1.72", patch_to="switch3:p02"),
                    "eno2": NetInterface(ip="192.168.1.73"),
                },
            ),
            "malina": NetworkNode(
                node_class="server",
                model="RaspberryPi",
                interfaces={
                    "eno1": NetInterface(ip="192.168.1.71", patch_to="switch3:p05"),
                    "docker0": NetInterface(ip="172.17.0.1"),
                },
            ),

        },
    )

    out_dir_path = os.path.join(os.path.dirname(__file__), "out")
    g = Grot(
        name="example_network",
        format="png",
        directory=out_dir_path,
        graph_attrs={"fontname": "Monospace", "splines": "true"},
        node_attrs={"fontname": "Monospace", "shape": "box", "margin": "0"},
        edge_attrs={"fontname": "Monospace", "penwidth": "1.80", "color": "#33000040"},
    )
    environment.draw_in_grot(g)
    g.render()

    # example ends here


if __name__ == "__main__":
    example_network()
