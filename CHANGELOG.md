## Grot's Changelog

## 0.1.0 - 2019.03.02
 Initial release. Gives automation for 
  * nodes creation 
  * short unique id calculation 
  * unlimited edges chain with single statement
  * inline edges labels definition
  * implicit nodes creation while creating edges
  * automation support for sub-graphs

## 0.1.1 - 2019.03.24
 Adding examples with exdoc automation prototype (no, got discontinued) 

## 0.1.2 - 2019.04.28
 Just a refactor and some cleanup. 

## 0.2.0 - 2022.10.10
  * Break `python2` compatibility.
  * Bump dependencies versions.
  * Add `Grot.html_node` method.
  * Remove the readme in RST format.